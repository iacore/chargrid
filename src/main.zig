const std = @import("std");
const janet = @import("janet");

const HTTP404 = std.http.Status.not_found;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    defer arena.deinit();

    var server = std.http.Server.init(gpa.allocator(), .{ .reuse_address = true });
    defer server.deinit();
    const addr = std.net.Address.parseIp("127.0.0.1", 1234) catch unreachable;
    try server.listen(addr);
    std.debug.print("Listening on http://{}\n", .{addr});

    var counter_man = try CounterManager.init(gpa.allocator());
    defer counter_man.deinit();

    while (true) {
        _ = arena.reset(.retain_capacity);
        var response = try server.accept(.{
            .allocator = arena.allocator(),
        });
        defer response.deinit();
        try response.wait();
        const path = response.request.target;
        // var buffer = [_]u8{0} ** 1024;
        // _ = try response.readAll(&buffer);

        var response_body: []const u8 = &.{};
        switch (response.request.method) {
            .GET => {
                if (std.mem.eql(u8, path, "/")) {
                    response_body = try std.fmt.allocPrint(arena.allocator(),
                        \\<script src="htmx.min.js"></script>
                        \\<h1>Hi</h1>
                        \\<button hx-post="/inc" hx-swap="innerHTML" hx-target="#counter">+</button>
                        \\<div id="counter">{}</div>
                        \\<button hx-post="/dec" hx-swap="innerHTML" hx-target="#counter">-</button>
                    , .{try counter_man.get()});
                } else if (std.mem.eql(u8, path, "/htmx.min.js")) {
                    const file = try std.fs.cwd().openFile("public/htmx.min.js", .{});
                    response_body = try file.readToEndAlloc(arena.allocator(), 99999);
                } else {
                    response.status = HTTP404;
                }
            },
            .POST => {
                if (std.mem.eql(u8, path, "/inc")) {
                    try counter_man.inc();
                    response_body = try std.fmt.allocPrint(arena.allocator(), "{}", .{try counter_man.get()});
                } else if (std.mem.eql(u8, path, "/dec")) {
                    try counter_man.dec();
                    response_body = try std.fmt.allocPrint(arena.allocator(), "{}", .{try counter_man.get()});
                } else {
                    response.status = HTTP404;
                }
            },
            else => {
                response.status = HTTP404;
            },
        }
        response.transfer_encoding = .{ .content_length = response_body.len };
        try response.do();
        try response.writeAll(response_body);
        try response.finish();
    }
}

const template_cell =
    \\<input type="text" name="cell_0_0" value="&#32;"
    \\    minlength="1" maxlength="1" hx-validate="true"
    \\    hx-get="/trigger_delay"
    \\    hx-trigger="keyup changed delay:500ms"
    \\    hx-target="#textgrid"
    \\>
;

fn emptyEnvorinment() *janet.Environment {
    return janet.Table.initDynamic(0).toEnvironment();
}

const CounterManager = struct {
    env: *janet.Environment,
    data: *janet.Table,

    pub fn init(a: std.mem.Allocator) !@This() {
        try janet.init();

        const core_env = janet.Environment.init(null);
        const env = emptyEnvorinment();
        env.proto = core_env;
        janet.gcRoot(env.toTable().wrap());

        const j_data = blk: {
            if (std.fs.cwd().readFileAlloc(a, "database", 20000)) |res| {
                defer a.free(res);
                var next: *const u8 = undefined;
                const stuff = janet.unmarshal(res, 0, emptyEnvorinment(), @ptrCast(&next));
                env.def("stuff", stuff, null);
                break :blk stuff;
            } else |_| {
                break :blk try env.doString("(def stuff @{:counter 0})", "(main)");
            }
        };

        return .{ .env = env, .data = j_data.unwrap(*janet.Table) catch |err| {
            std.log.err("Database has invalid format. Please remove database and relaunch the program.", .{});
            return err;
        } };
    }
    pub fn deinit(this: *@This()) void {
        _ = janet.gcUnroot(this.env.toTable().wrap());
        janet.deinit();
    }

    pub fn persist(this: *@This()) !void {
        const buf = janet.Buffer.initN("");

        janet.marshal(buf, this.data.wrap(), emptyEnvorinment(), 0);
        const f_staging = try std.fs.cwd().createFile("database~", .{ .lock = .exclusive });
        defer f_staging.close();
        try f_staging.writeAll(buf.data[0..@intCast(buf.count)]);
        try f_staging.sync();
        try std.fs.cwd().rename("database~", "database");
    }

    pub fn get(this: *@This()) !i32 {
        const count_v = try this.env.doString("(stuff :counter)", "(main)");
        const count = try count_v.unwrap(i32);
        return count;
    }
    pub fn inc(this: *@This()) !void {
        _ = try this.env.doString("(set (stuff :counter) (inc (stuff :counter)))", "(main)");
        try this.persist();
    }
    pub fn dec(this: *@This()) !void {
        _ = try this.env.doString("(set (stuff :counter) (dec (stuff :counter)))", "(main)");
        try this.persist();
    }
};
